Docker image build instructions for GEOS

See http://geos.osgeo.org

Here we put instructions to build docker images for different
purposes, initially for running build agents in isolation.

Each image configuration is in its own directory, which should
contain additional information about the specific image.

```
cd build-test
make build
#need to log into osgeo geos docker registry first using your OSGeo LDAP account
make login
make push
```

A general side note, the registry for pushing is geos-docker.osgeo.org, 
but the public registry (used for pulling by CI) is docker.osgeo.org.
